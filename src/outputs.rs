use std::net::UdpSocket;
use std::sync::mpsc::RecvError;
use crossbeam_channel::{Receiver, Select};
use crate::data::Message;
use crate::errors;
use crate::errors::ConnectionError;


pub struct UdpOutput {
    socket: UdpSocket,
    peers : Vec<String>,
    receivers: Vec<Receiver<Message>>
}

impl UdpOutput {
    pub fn new(receivers: Vec<Receiver<Message>>, peers: Vec<&str>) ->  Result<Self, errors::ConnectionError> {
        let socket = UdpSocket::bind("127.0.0.1:0");

        if socket.is_err() {
            return Err(ConnectionError::UdpBindingError {})
        }

        Ok(UdpOutput {
            socket: socket.unwrap(),
            peers: peers.to_vec().iter().map(|&p| p.to_string()).collect(),
            receivers
        })

    }

    pub fn run(&mut self) -> Result<(), RecvError> {

        let mut selector = Select::new();

        for receiver in &self.receivers {
            selector.recv(&receiver);
        }

        loop {
            let index = selector.ready();
            let result = self.receivers[index].try_recv();

            match result {
                Ok(result) => {
                    for peer in &self.peers {
                        let r = self.socket.send_to(&result.buf, peer);

                        if let Err(_e) = r {

                        }
                    }
                },
                Err(e) => {
                    if e.is_empty() {
                        continue;
                    }
                }
            };

        }
    }
}
