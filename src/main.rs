use std::thread;
use stream_duplicator::{inputs, outputs};
use stream_duplicator::inputs::TCPInput;

fn main() {
    println!("Hello, world!");

    let (tx, rx) = TCPInput::create_channels();

    thread::spawn(move ||{
        let mut client = inputs::TCPInput::new("127.0.0.1:3034").unwrap();
        client.set_sender(tx);
        client.run()
    });

    let rx1 = rx.clone();

    let handle = thread::spawn(move ||{
        let mut peer1 = outputs::UdpOutput::new(
            vec![rx],
            vec![
                "127.0.0.1:5000",
                "127.0.0.1:5002",
                "127.0.0.1:5008"]).unwrap();
        peer1.run()
    });

    let handle2 = thread::spawn(move ||{
        let mut peer2 = outputs::UdpOutput::new(
            vec![rx1],
            vec![
                "127.0.0.1:5010"]).unwrap();
        peer2.run()
    });

    handle.join().unwrap();
    handle2.join().unwrap();
}
